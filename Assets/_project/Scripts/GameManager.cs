using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public event EventHandler OnStateChanged;
    public event EventHandler OnGamePaused;
    public event EventHandler OnGameUnPaused;

    [SerializeField] private float _waitingToStartTimer = 0.1f;
    [SerializeField] private float _countDownTOStartTimer = 5f;
    [SerializeField] private float _gamePlayingTimerMax = 5f;
    private float _gamePlayingTimer;
    private bool _isGamePaused = false;

    private State _state;
    

    private enum State
    {
        WaitingToStart,
        CountDownToStart,
        GamePlaying,
        GameOver,

    }
    private void Start()
    {
        GameInput.Instance.OnPausePressed += GameInput_OnPausePressed;
        GameInput.Instance.OnInteractAction += GameInput_OnInteractAction;
    }

    private void GameInput_OnInteractAction(object sender, EventArgs e)
    {
        if (_state == State.WaitingToStart)
        {
            _state = State.CountDownToStart;
            OnStateChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    private void GameInput_OnPausePressed(object sender, EventArgs e)
    {
        TogglePauseGame();
    }

    private void Awake()
    {
        Instance = this;
        _state = State.WaitingToStart;
    }

    private void Update()
    {
        switch (_state)
        {
            case State.WaitingToStart:
                break;
            case State.CountDownToStart:
                _countDownTOStartTimer -= Time.deltaTime;
                if (_countDownTOStartTimer < 0f)
                {
                    _state = State.GamePlaying;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                    _gamePlayingTimer = _gamePlayingTimerMax;
                }
                
                break;
            case State.GamePlaying:
                _gamePlayingTimer -= Time.deltaTime;
                if (_gamePlayingTimer < 0f)
                {
                    _state = State.GameOver;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }

                break;
            case State.GameOver:

                break;
        }
        Debug.Log(_state);
    }

    public float GetPlayingTimerNormalized()
    {

        return 1 - (_gamePlayingTimer / _gamePlayingTimerMax);
    }


    public float GetCountDownTimer()
    {
        return _countDownTOStartTimer;
    }

    public bool IsGamePlaying()
    {
        return _state == State.GamePlaying; 
    }

    public bool IsCountDowntToStartActive()
    {
        return _state == State.CountDownToStart;
    }

    public bool IsGameOver()
    {
        return _state == State.GameOver;
    }

    public void TogglePauseGame()
    {
        _isGamePaused = !_isGamePaused; 
        if (_isGamePaused)
        {
            Time.timeScale = 0f;
            OnGamePaused?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            Time.timeScale = 1f;
            OnGameUnPaused?.Invoke(this, EventArgs.Empty);
        }
    }

}
