using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlateCompleteVisual : MonoBehaviour
{
    [Serializable]
    public struct KitchenObjectSO_GameObject
    {
        public GameObject GameObject;
        public KitchenObjSO KitchenObjSO;
    }

    [SerializeField] private PlateKitchenObject _plateKitchenObject;
    [SerializeField] private List<KitchenObjectSO_GameObject> _kitchenObjectSO_GameObjectList;

    

    private void Start()
    {
        _plateKitchenObject.OnIngridentAdded += _plateKitchenObject_OnIngridentAdded;

        foreach (KitchenObjectSO_GameObject kitchenObjectSO_GameObject in _kitchenObjectSO_GameObjectList)
        {
            kitchenObjectSO_GameObject.GameObject.SetActive(false);
        }
    }

    private void _plateKitchenObject_OnIngridentAdded(object sender, PlateKitchenObject.OnIngridentAddedEventArgs e) // ������� ���� � ����� ����������, ���������
                                                                                                                    // �� � ����������
    {
        foreach (KitchenObjectSO_GameObject kitchenObjectSO_GameObject in _kitchenObjectSO_GameObjectList)      // ����� �� �����
        {
            if (kitchenObjectSO_GameObject.KitchenObjSO == e.kitchenObjSOEvent)                                 // ���� KitchenObjSO �� ����� ����� ���������� � ������
            {
                kitchenObjectSO_GameObject.GameObject.SetActive(true);                                      // ��������
            }
        }
    }
}
