using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarCuttingUI : MonoBehaviour
{
    [SerializeField] private Image _barImage;
    [SerializeField] private GameObject _IhasProgressGameObject;

    private IhasProgress _IhasProgress;

    private void Start()
    {
        _IhasProgress = _IhasProgressGameObject.GetComponent<IhasProgress>();
        if (_IhasProgress == null)
        {
            Debug.LogError("ON GO dont have interface IhasProgress");
        }
        _IhasProgress.OnProgressChanged += _IhasProgress_OnProgressOfCuttingChanged;
        _barImage.fillAmount = 0f;
        Hide();
    }

    private void _IhasProgress_OnProgressOfCuttingChanged(object sender, IhasProgress.OnProgressChangedEventArgs e)
    {
        _barImage.fillAmount = e.ProgressEventNormalized;
        if (e.ProgressEventNormalized == 0f || e.ProgressEventNormalized == 1f)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject?.SetActive(false);
    }

}
