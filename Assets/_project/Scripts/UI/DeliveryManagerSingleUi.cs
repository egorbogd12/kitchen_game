using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DeliveryManagerSingleUi : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private Transform _iconContainer;
    [SerializeField] private Transform _iconTemplate;


    private void Awake()
    {
        _iconTemplate.gameObject.SetActive(false);
    }
    public void SetRecipeSO(RecipeSO recipeSO)
    {
        _nameText.text = recipeSO.name;
        foreach (Transform child in _iconContainer)
        {
            if (child == _iconTemplate)
            {
                continue;
            }
            Destroy(child.gameObject);
        }

        foreach (KitchenObjSO kitchenObjSO in recipeSO.KitchenObjSOList)
        {
            Transform icontransform =  Instantiate(_iconTemplate, _iconContainer);
            icontransform.gameObject.SetActive(true);
            icontransform.GetComponent<Image>().sprite = kitchenObjSO.sprite;
        }
    }

}
    
