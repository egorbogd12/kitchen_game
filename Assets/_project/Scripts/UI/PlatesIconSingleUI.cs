using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatesIconSingleUI : MonoBehaviour
{
    [SerializeField] private Image _image;

    public void SetKitchenObjSO( KitchenObjSO kitchenObjSO )
    {
        _image.sprite = kitchenObjSO.sprite;
    }

}
