using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateIconsUI : MonoBehaviour
{
    [SerializeField] private PlateKitchenObject _plateKitchenObject;
    [SerializeField] private Transform _iconTemplate;

    private void Awake()
    {
        _iconTemplate.gameObject.SetActive(false);
    }

    private void Start()
    {
        _plateKitchenObject.OnIngridentAdded += _plateKitchenObject_OnIngridentAdded;
    }

    private void _plateKitchenObject_OnIngridentAdded(object sender, PlateKitchenObject.OnIngridentAddedEventArgs e)
    {
        UpdateVisual();
    }
    private void UpdateVisual() // c������ ����� � ������
    {
        foreach (Transform child in transform)
        {
            if (child == _iconTemplate) continue;
            Destroy(child.gameObject);             // ������� ��� �������� ������� (����� _iconTemplate) ������������ �������, �� ������� ���������� ������ ���������. 
        }
        foreach (KitchenObjSO kitchenObjSO in _plateKitchenObject.GetKitchenObjSOList())
        {
            Transform iconTransform = Instantiate(_iconTemplate , transform);
            iconTransform.GetComponent<PlatesIconSingleUI>().SetKitchenObjSO(kitchenObjSO);
            iconTransform.gameObject.SetActive(true);
        }
    }
}
