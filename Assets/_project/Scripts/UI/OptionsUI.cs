using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class OptionsUI : MonoBehaviour
{
    public static OptionsUI Instance { get; private set; }

    [SerializeField] private Transform _rebindKeyCanvas;
    [SerializeField] private Button _soundEffectsButton;
    [SerializeField] private Button _musicButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _moveUpButton;
    [SerializeField] private Button _moveDownButton;
    [SerializeField] private Button _moveLeftButton;
    [SerializeField] private Button _moveRightButton;
    [SerializeField] private Button _interactButton;
    [SerializeField] private Button _interactAltButton;
    [SerializeField] private Button _pauseButton;
    [SerializeField] private Button _gamepadInteractButton;
    [SerializeField] private Button _gamepadInteractAltButton;
    [SerializeField] private Button _gamepadPauseButton;
    [SerializeField] private TextMeshProUGUI _soundsEffectsText;
    [SerializeField] private TextMeshProUGUI _musicText;
    [SerializeField] private TextMeshProUGUI _moveUpText;
    [SerializeField] private TextMeshProUGUI _moveDownText;
    [SerializeField] private TextMeshProUGUI _moveLeftText;
    [SerializeField] private TextMeshProUGUI _moveRightText;
    [SerializeField] private TextMeshProUGUI _interactText;
    [SerializeField] private TextMeshProUGUI _interactAltText;
    [SerializeField] private TextMeshProUGUI _pauseText;
    [SerializeField] private TextMeshProUGUI _gamepadInteractText;
    [SerializeField] private TextMeshProUGUI _gamepadInteractAltText;
    [SerializeField] private TextMeshProUGUI _gamepadPauseText;

    private Action _OnCloseButtonAction;

    private void Awake()
    {
        Instance = this;
        _soundEffectsButton.onClick.AddListener(SoundEffectButton);
        _musicButton.onClick.AddListener(MusicButton);
        _closeButton.onClick.AddListener(Hide);

        _moveUpButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.MoveUp); });
        _moveDownButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.MoveDown); });
        _moveLeftButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.MoveLeft); });
        _moveRightButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.MoveRight); });
        _interactButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Interact); });
        _interactAltButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.InteractAlternate); });
        _pauseButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Pause); });
        _gamepadInteractButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.GamePad_Interact); });
        _gamepadInteractAltButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.GamePad_InteractAlternate); });
        _gamepadPauseButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.GamePad_Pause); });
    }
    private void Start()
    {
        GameManager.Instance.OnGameUnPaused += GameManager_OnGameUnPaused;
        HideRebindCanvas();
        UpdateVisual();
        Hide();
    }

    private void GameManager_OnGameUnPaused(object sender, System.EventArgs e)
    {
        Hide();
    }

    private void MusicButton()
    {
        MusicManager.Instance.ChangeVolume();
        UpdateVisual();
    }

    private void SoundEffectButton()
    {
        SoundManager.Instance.ChangeVolume();
        UpdateVisual();
    }
    private void UpdateVisual()
    {
        _soundsEffectsText.text = "Sounds effects:" + Mathf.Round(SoundManager.Instance.GetVolume() * 10f);
        _musicText.text = "Music:" + Mathf.Round(MusicManager.Instance.GetVolume() * 10f);
        _moveUpText.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveUp);
        _moveDownText.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveDown);
        _moveLeftText.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveLeft);
        _moveRightText.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveRight);
        _interactText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Interact);
        _interactAltText.text = GameInput.Instance.GetBindingText(GameInput.Binding.InteractAlternate);
        _pauseText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Pause);
        _gamepadInteractText.text = GameInput.Instance.GetBindingText(GameInput.Binding.GamePad_Interact);
        _gamepadInteractAltText.text = GameInput.Instance.GetBindingText(GameInput.Binding.GamePad_InteractAlternate);
        _gamepadPauseText.text = GameInput.Instance.GetBindingText(GameInput.Binding.GamePad_Pause);

    }

    public void Show()
    {
        gameObject.SetActive(true);
        _soundEffectsButton.Select();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void ShowRebindCanvas()
    {
        _rebindKeyCanvas.gameObject.SetActive(true);
    }
    public void HideRebindCanvas()
    {
        _rebindKeyCanvas.gameObject.SetActive(false);

    }

    private void RebindBinding(GameInput.Binding binding)
    {
        ShowRebindCanvas();
        GameInput.Instance.RebindBindings(binding, () =>
        {
            HideRebindCanvas();
            UpdateVisual();
        });
    }

}
