using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textUP;
    [SerializeField] private TextMeshProUGUI _textDown;
    [SerializeField] private TextMeshProUGUI _textLeft;
    [SerializeField] private TextMeshProUGUI _textRight;
    [SerializeField] private TextMeshProUGUI _interact;
    [SerializeField] private TextMeshProUGUI _interactAlt;
    [SerializeField] private TextMeshProUGUI _pause;

    private void Start()
    {
        GameInput.Instance.OnBindingsRebind += GameInput_OnBindingsRebind;
        UpdateVisual();
        GameManager.Instance.OnStateChanged += GameManager_OnStateChanged;
    }

    private void GameManager_OnStateChanged(object sender, System.EventArgs e)
    {
        if (GameManager.Instance.IsCountDowntToStartActive())
        {
            Hide();
        }
    }

    private void GameInput_OnBindingsRebind(object sender, System.EventArgs e)
    {
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        _textUP.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveUp);
        _textDown.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveDown);
        _textLeft.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveLeft);
        _textRight.text = GameInput.Instance.GetBindingText(GameInput.Binding.MoveRight);
        _interact.text = GameInput.Instance.GetBindingText(GameInput.Binding.Interact);
        _interactAlt.text = GameInput.Instance.GetBindingText(GameInput.Binding.InteractAlternate);
        _pause.text = GameInput.Instance.GetBindingText(GameInput.Binding.Pause);

    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
