using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoveBurnUi : MonoBehaviour
{
    [SerializeField] private StoveCounter _stoveCounter;

    private void Start()
    {
        _stoveCounter.OnProgressChanged += _stoveCounter_OnProgressChanged;
        Hide();
    }

    private void _stoveCounter_OnProgressChanged(object sender, IhasProgress.OnProgressChangedEventArgs e)
    {
        float burnshowProgressAmmount = .4f;
        
        bool show = _stoveCounter.IsFried() && e.ProgressEventNormalized >= burnshowProgressAmmount;
        if (show)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);

    }
}
