using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePauseUI : MonoBehaviour
{
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _mainMenuButton;
    [SerializeField] private Button _optionsButton;

    private void Awake()
    {
        _resumeButton.onClick.AddListener(ResumeButton);
        _mainMenuButton.onClick.AddListener(MainMenuButton);
        _optionsButton.onClick.AddListener(OptionsButton);
    }
    private void Start()
    {
        GameManager.Instance.OnGamePaused += GameManager_OnGamePaused;
        GameManager.Instance.OnGameUnPaused += GameManager_OnGameUnPaused;
        Hide();
    }

    private void GameManager_OnGameUnPaused(object sender, System.EventArgs e)
    {
        Hide();
    }

    private void GameManager_OnGamePaused(object sender, System.EventArgs e)
    {
        Show();
    }

    private void Show()
    {
        gameObject.SetActive(true);
        _resumeButton.Select(); 
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void MainMenuButton()
    {
        Loader.Load(Loader.Scene.MainMenuScene);
    }
    private void ResumeButton()
    {
        GameManager.Instance.TogglePauseGame();
    }

    private void OptionsButton()
    {
        Hide();
        OptionsUI.Instance.Show();
    }
}
