using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _quitButton;

    private void Awake()
    {
        _playButton.onClick.AddListener(OnClickPlay);
        _quitButton.onClick.AddListener(OnClickQUit);
        Time.timeScale = 1f;
    }

    private void OnClickPlay()
    {
        Loader.Load(Loader.Scene.MainScene);
    }

    private void OnClickQUit()
    {
        Application.Quit();
    }
}
