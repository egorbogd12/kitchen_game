using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoveBurnFlashUI : MonoBehaviour
{
    private const string IS_FLASHING = "IsFlashing";
    [SerializeField] private StoveCounter _stoveCounter;
    private Animator _animator;
    private Image _image;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.SetBool(IS_FLASHING, false);
    }

    private void Start()
    {
        _stoveCounter.OnProgressChanged += _stoveCounter_OnProgressChanged;
        _animator.SetBool(IS_FLASHING, false);
    }

    private void _stoveCounter_OnProgressChanged(object sender, IhasProgress.OnProgressChangedEventArgs e)
    {
        float burnshowProgressAmmount = .4f;

        bool show = _stoveCounter.IsFried() && e.ProgressEventNormalized >= burnshowProgressAmmount;

        if (show)
        {

            _animator.SetBool(IS_FLASHING, true);
        }
        else
        {
            _animator.SetBool(IS_FLASHING, false);


        }
    }
}
