using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlatesCounter : BaseCounter
{
    [SerializeField, Range(1,10)] float _spawnCD = 4f;
    [SerializeField, Range(1, 10)] int _platesMaxAmmount = 4;
    [SerializeField] private KitchenObjSO _plateKitchenObjSO;
    private float _spawnPlateTimer;
    private int _platesSpawned;

    public event EventHandler OnPlateSpawned;
    public event EventHandler OnPlateRemoved;

    private void Update()
    {
        _spawnPlateTimer += Time.deltaTime;
        if (_spawnPlateTimer >= _spawnCD)
        {
            _spawnPlateTimer = 0f;
            if (GameManager.Instance.IsGamePlaying() && _platesSpawned < _platesMaxAmmount)
            {
                _platesSpawned++;
                OnPlateSpawned?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    public override void Interact(Player player)
    {
        if (!player.HasKitchenObject())
        {
            // ��� � ����� ������
            if (_platesSpawned > 0) // ���� ���� ���� �������
            {
                _platesSpawned--;
                
                KitchenObject.SpawnKitchenObject(_plateKitchenObjSO, player);
                OnPlateRemoved?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
