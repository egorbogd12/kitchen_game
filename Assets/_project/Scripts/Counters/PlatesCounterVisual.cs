using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesCounterVisual : MonoBehaviour
{
    [SerializeField] private Transform _topPoint;
    [SerializeField] private Transform _platePrefab;
    [SerializeField] private PlatesCounter _platesCounter;

    private List<GameObject> _plateGameObjectList;
    private void Awake()
    {
        _plateGameObjectList = new List<GameObject>();
    }

    private void Start()
    {
        _platesCounter.OnPlateSpawned += _platesCounter_OnPlateSpawned;
        _platesCounter.OnPlateRemoved += _platesCounter_OnPlateRemoved;
    }

    private void _platesCounter_OnPlateRemoved(object sender, System.EventArgs e)
    {
        GameObject plate = _plateGameObjectList[_plateGameObjectList.Count - 1];
        _plateGameObjectList.Remove(plate);
        Destroy(plate);
    }

    private void _platesCounter_OnPlateSpawned(object sender, System.EventArgs e)
    {
      Transform plateTransform =  Instantiate(_platePrefab, _topPoint);
        float offsetPlate = .1f;

        plateTransform.localPosition = new Vector3(0f, offsetPlate * _plateGameObjectList.Count, 0f);

        _plateGameObjectList.Add(plateTransform.gameObject);
    }
}
