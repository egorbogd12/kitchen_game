using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StoveCounter : BaseCounter, IhasProgress
{
    public event EventHandler<IhasProgress.OnProgressChangedEventArgs> OnProgressChanged;
    public event EventHandler <OnStateChangedEventArgs> OnStateChanged;

    public class OnStateChangedEventArgs : EventArgs
    {
        public State _stateEvent;
    }
    public enum State 
    {
        Idle,
        Firing,
        Fired,
        Burned,
    }
    
    private State _state;

    [SerializeField] private FiringRecipeSO[] _firingRecipeSOArray;
    [SerializeField] private BurningRecipeSO[] _burningRecipeSOArray;

    private float _firiingTimer;
    private float _burningTimer;
    private FiringRecipeSO _firingRecipeSO;
    private BurningRecipeSO _burningRecipeSO;



    private void Start()
    {
        _state = State.Idle;
    }
    private void Update()
    {
        if (HasKitchenObject())
        {
            switch (_state)
            {
                case State.Idle:
                    break;
                case State.Firing:
                    _firiingTimer += Time.deltaTime;
                    OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                    {
                        ProgressEventNormalized = _firiingTimer / _firingRecipeSO.FiringProgressMax
                    });
                    if (_firiingTimer > _firingRecipeSO.FiringProgressMax)
                    {
                        GetKitchenObject().DestroySelf();
                        KitchenObject.SpawnKitchenObject(_firingRecipeSO._objOutput, this);
                        _state = State.Fired;
                        _burningTimer = 0f;
                        _burningRecipeSO = GetBurningSOWithInput(GetKitchenObject().GetKitchenObjSO());
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs { _stateEvent = _state});

                    }
                    break;
                case State.Fired:
                    _burningTimer += Time.deltaTime;
                    OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                    {
                        ProgressEventNormalized = _burningTimer / _burningRecipeSO.BurningProgressMax
                    });
                    if (_burningTimer > _burningRecipeSO.BurningProgressMax)
                    {
                        GetKitchenObject().DestroySelf();
                        KitchenObject.SpawnKitchenObject(_burningRecipeSO._objOutput, this);
                        _state = State.Burned;
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs { _stateEvent = _state });
                        // gotovo 
                    }
                    break;
                case State.Burned:
                    _burningTimer = 0;
                    OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                    {
                        ProgressEventNormalized = _burningTimer / _burningRecipeSO.BurningProgressMax
                    });
                    break;
            }

        }
    }

    public override void Interact(Player player)
    {
        if (!HasKitchenObject()) // �������� �� ������� �����(clearCounter)
        {
            if (player.HasKitchenObject()) // ���� �� � ����� 
            {
                KitchenObject inputKitchenObject = player.GetKitchenObject();
                if (HasRecipeInput(inputKitchenObject.GetKitchenObjSO()))
                {
                    inputKitchenObject.SetKitchenObjectParent(this);// ������ �� �����
                    _firingRecipeSO = GetFiringSOWithInput(inputKitchenObject.GetKitchenObjSO());
                    _state = State.Firing;
                    OnStateChanged?.Invoke(this, new OnStateChangedEventArgs { _stateEvent = _state });
                    _firiingTimer = 0f;
                    OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs { 
                        ProgressEventNormalized = _firiingTimer / _firingRecipeSO.FiringProgressMax });
                    

                    // player.GetKitchenObject().SetKitchenObjectParent(this); // ������ �� �����
                }
            }
        }
        else
        {
            if (player.HasKitchenObject())
            {
                // ������ �� ������ �����
                if (player.GetKitchenObject().TryGetPlate(out PlateKitchenObject platekitchenobject)) // �������� ��� ����� ������ ������� � �����    
                {
                    PlateKitchenObject plateKitchenObject = player.GetKitchenObject() as PlateKitchenObject;
                    if (plateKitchenObject.TryAddIngridient(GetKitchenObject().GetKitchenObjSO()))
                    {
                        GetKitchenObject().DestroySelf();
                        _state = State.Idle;
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs { _stateEvent = _state });
                        OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                        {
                            ProgressEventNormalized = 0f
                        });
                    }
                }
            }
            else // � ����� ��� . �������� �����
            {
                GetKitchenObject().SetKitchenObjectParent(player);
                _state = State.Idle;
                OnStateChanged?.Invoke(this, new OnStateChangedEventArgs { _stateEvent = _state });
                OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                {
                    ProgressEventNormalized = 0f
                });
            }
        }
    }

    private bool HasRecipeInput(KitchenObjSO inputKitchenObjSO)
    {
        FiringRecipeSO firingRecipeSO = GetFiringSOWithInput(inputKitchenObjSO);
        return firingRecipeSO != null;
    }

    private KitchenObjSO GetOutputForInput(KitchenObjSO inputKitchenObjSO)
    {
        FiringRecipeSO firingRecipeSO = GetFiringSOWithInput(inputKitchenObjSO);
        if (firingRecipeSO != null)
        {
            return firingRecipeSO._objOutput;
        }
        return null;
    }

    private FiringRecipeSO GetFiringSOWithInput(KitchenObjSO inputKitchenObjSO)
    {
        foreach (FiringRecipeSO firingRecipeSO in _firingRecipeSOArray)
        {
            if (firingRecipeSO._objInput == inputKitchenObjSO)
            {
                return firingRecipeSO;
            }
        }
        return null;
    }

    private BurningRecipeSO GetBurningSOWithInput(KitchenObjSO inputKitchenObjSO)
    {
        foreach (BurningRecipeSO burningRecipeSO in _burningRecipeSOArray)
        {
            if (burningRecipeSO._objInput == inputKitchenObjSO)
            {
                return burningRecipeSO;
            }
        }
        return null;
    }

    public bool IsFried()
    {
        return _state == State.Fired;
    }
}
