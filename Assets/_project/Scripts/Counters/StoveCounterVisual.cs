using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoveCounterVisual : MonoBehaviour
{
    [SerializeField] private GameObject _stoveOnGameObject;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private StoveCounter _stoveCounter;
    private void Start()
    {
        _stoveCounter.OnStateChanged += _stoveCounter_OnStateChanged;
    }

    private void _stoveCounter_OnStateChanged(object sender, StoveCounter.OnStateChangedEventArgs e)
    {

        if (e._stateEvent == StoveCounter.State.Firing || e._stateEvent == StoveCounter.State.Fired)
        {
            _particleSystem.Play();
            _stoveOnGameObject.SetActive(true);
        }
        else
        {
            _particleSystem.Stop();
            _stoveOnGameObject.SetActive(false);
        }
    }
}
