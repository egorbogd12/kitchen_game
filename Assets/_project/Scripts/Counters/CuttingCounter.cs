using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CuttingCounter : BaseCounter, IhasProgress
{
    public event EventHandler<IhasProgress.OnProgressChangedEventArgs> OnProgressChanged;

    public event EventHandler OnCut;
    public static event EventHandler OnAnyCut;
    new public static void ResetStaticData()
    {
        OnAnyCut = null;
    }


    [SerializeField] private CuttingRecipeSO[] _cuttingRecipesSOArray;
    private int _cuttingProgress;

    public override void Interact(Player player)
    {
        if (!HasKitchenObject()) // �������� �� ������� �����(clearCounter)
        {
            if (player.HasKitchenObject()) // ���� �� � ����� 
            {
                KitchenObject inputKitchenObject = player.GetKitchenObject();
                if (HasRecipeInput(inputKitchenObject.GetKitchenObjSO()))
                {
                    inputKitchenObject.SetKitchenObjectParent(this);// ������ �� �����
                    _cuttingProgress = 0;
                    CuttingRecipeSO cuttingRecipeSO = GetRecipeSOWithInput(GetKitchenObject().GetKitchenObjSO());
                    OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
                    {
                        ProgressEventNormalized = (float)_cuttingProgress / cuttingRecipeSO.cuttingProgressMax
                    });
                }
                ;
                // player.GetKitchenObject().SetKitchenObjectParent(this); // ������ �� �����
            }
        }
        else
        {
            if (player.HasKitchenObject())
            {
                if (player.GetKitchenObject().TryGetPlate(out PlateKitchenObject platekitchenobject)) // �������� ��� ����� ������ ������� � �����    
                {
                    PlateKitchenObject plateKitchenObject = player.GetKitchenObject() as PlateKitchenObject;
                    if (plateKitchenObject.TryAddIngridient(GetKitchenObject().GetKitchenObjSO()))
                    {
                        GetKitchenObject().DestroySelf();
                    }
                }
            }
            else // � ����� ��� . �������� �����
            {
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }

    public override void InteractAlternate(Player player)
    {
        if (HasKitchenObject() && HasRecipeInput(GetKitchenObject().GetKitchenObjSO())) // ���� �� ������ ������� ����� �������� � �� ���� � ��������
        {
            CuttingRecipeSO cuttingRecipeSO = GetRecipeSOWithInput(GetKitchenObject().GetKitchenObjSO());

            _cuttingProgress++;
            OnCut?.Invoke(this, EventArgs.Empty);
            OnAnyCut?.Invoke(this, EventArgs.Empty);
            OnProgressChanged?.Invoke(this, new IhasProgress.OnProgressChangedEventArgs
            {
                ProgressEventNormalized = (float)_cuttingProgress / cuttingRecipeSO.cuttingProgressMax
            });


            if (_cuttingProgress >= cuttingRecipeSO.cuttingProgressMax)
            {
                KitchenObjSO outputKitchenObjSO = GetOutputForInput(GetKitchenObject().GetKitchenObjSO());
                GetKitchenObject().DestroySelf();

                KitchenObject.SpawnKitchenObject(outputKitchenObjSO, this);
            }
        }
    }

    private bool HasRecipeInput(KitchenObjSO inputKitchenObjSO)
    {
        CuttingRecipeSO cuttingRecipeSO = GetRecipeSOWithInput(inputKitchenObjSO);
        return cuttingRecipeSO != null;
    }

    private KitchenObjSO GetOutputForInput(KitchenObjSO inputKitchenObjSO)
    {
        CuttingRecipeSO cuttingRecipeSO = GetRecipeSOWithInput(inputKitchenObjSO);
        if (cuttingRecipeSO != null)
        {
            return cuttingRecipeSO._objOutput;
        }
        return null;
    }

    private CuttingRecipeSO GetRecipeSOWithInput(KitchenObjSO inputKitchenObjSO)
    {
        foreach (CuttingRecipeSO cuttingRecipeSO in _cuttingRecipesSOArray)
        {
            if (cuttingRecipeSO._objInput == inputKitchenObjSO)
            {
                return cuttingRecipeSO;
            }
        }
        return null;
    }

}
