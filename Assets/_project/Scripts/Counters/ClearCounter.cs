using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCounter : BaseCounter
{
    [SerializeField] private KitchenObjSO _kitchenObjSO;

    public override void Interact( Player player) // �������������� � ������(clearCounter)
    {
        if (!HasKitchenObject()) // �������� �� ������� �����(clearCounter)
        {
            if (player.HasKitchenObject())
            {
                player.GetKitchenObject().SetKitchenObjectParent(this);
            }
        }
        else
        {
            if (player.HasKitchenObject())
            {
                if (player.GetKitchenObject().TryGetPlate( out PlateKitchenObject platekitchenobject)) // �������� ��� ����� ������ ������� � �����    
                {
                    PlateKitchenObject plateKitchenObject = player.GetKitchenObject() as PlateKitchenObject;
                    if (plateKitchenObject.TryAddIngridient(GetKitchenObject().GetKitchenObjSO()))
                    {
                        GetKitchenObject().DestroySelf();
                    }
                }
                else // ����� ������ �� ������� � ��� �� ������
                {
                    if (GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject)) // ������� ����� �� ����� (Counter) 
                    {
                        if (plateKitchenObject.TryAddIngridient(player.GetKitchenObject().GetKitchenObjSO()))
                        {
                            player.GetKitchenObject().DestroySelf();
                        } 
                    }
                }
            }
            else
            {
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }
    
}

