using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ContainerCounter : BaseCounter
{
    public event EventHandler OnPlayerGrabbedItem;

    [SerializeField] private KitchenObjSO _kitchenObjSO;

    public override void Interact(Player player) // �������������� � �������
    {
        if (!player.HasKitchenObject()) // �������� �� �� , ��� ��� ������� �� �����. ���� ��� , �� �������
        {
            KitchenObject.SpawnKitchenObject(_kitchenObjSO,player);
            OnPlayerGrabbedItem?.Invoke(this,EventArgs.Empty);
        }
    }
}
