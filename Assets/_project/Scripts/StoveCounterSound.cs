using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoveCounterSound : MonoBehaviour
{
    [SerializeField] private StoveCounter _stoveCounter;

    private AudioSource _audioSource;

    private float _warningTimerSound = .2f;
    private bool _playWarningSound = false;
    private void Start()
    {
        _stoveCounter.OnStateChanged += _stoveCounter_OnStateChanged;
        _stoveCounter.OnProgressChanged += _stoveCounter_OnProgressChanged;
    }

    private void _stoveCounter_OnProgressChanged(object sender, IhasProgress.OnProgressChangedEventArgs e)
    {

        float burnshowProgressAmmount = .4f;

        _playWarningSound = _stoveCounter.IsFried() && e.ProgressEventNormalized >= burnshowProgressAmmount;
        
    }

    private void _stoveCounter_OnStateChanged(object sender, StoveCounter.OnStateChangedEventArgs e)
    {
        bool playSound = e._stateEvent == StoveCounter.State.Firing || e._stateEvent == StoveCounter.State.Fired;
        if (playSound)
        {
            StoveCounter stoveCounter = sender as StoveCounter;
            _audioSource.Play();
            //AudioSource.PlayClipAtPoint(_audioClip,stoveCounter.transform.position);
        }
        else
        {
            _audioSource.Pause();
        }

    }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (_playWarningSound)
        { 
            _warningTimerSound -= Time.deltaTime;
            if (_warningTimerSound <= 0f)
            {
                float warningTimerSoundMax = .2f;
                _warningTimerSound = warningTimerSoundMax;
                SoundManager.Instance.PlayWarningSound(this.transform.position);
            }
        }

    }
}
