using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(PlayerMovement))]

public class Player : MonoBehaviour , IKitchenObjectParent
{
    public static Player Instance { get; private set; }

    [SerializeField] private LayerMask _countersLayerMask;
    [SerializeField] private Transform _kitchenObjectTopPoint;

    private bool _isWalking;
    private PlayerMovement _playerMovement;
    private Vector3 _lastMoveDirection;
    private KitchenObject _kitchenObject;
    private BaseCounter _SelectedCounterInPlayer;

    public event EventHandler OnPickedSomething;
    public event EventHandler <OnSelectedCounterChangedEventArgs> OnSelectedCounterChanged;
    public class OnSelectedCounterChangedEventArgs : EventArgs
    {
        public BaseCounter _SelectedCounterInEvent;
    }
    

    private void Awake()
    {
        _playerMovement = GetComponent<PlayerMovement>();
        if (Instance != null)
        {
            Debug.LogError("More than 1 player instance");
        }

        Instance = this;
    }

    private void Start()
    {
        GameInput.Instance.OnInteractAction += GameInput_OnInteractAction;
        GameInput.Instance.OnInteactAlternate += GameInput_OnInteactAlternate;
    }

    private void GameInput_OnInteactAlternate(object sender, EventArgs e)
    {
        if (GameManager.Instance.IsGamePlaying())
        {
            if (_SelectedCounterInPlayer != null)
            {
                _SelectedCounterInPlayer.InteractAlternate(this);
            }
        }
        
    }

    public void GameInput_OnInteractAction(object sender, System.EventArgs e)
    {
        if (GameManager.Instance.IsGamePlaying())
        {
            if (_SelectedCounterInPlayer != null)
            {
                _SelectedCounterInPlayer.Interact(this);
            }
        }
        
    }

    private void Update()
    {
        IsWalking();
        HandleInteractions();
    }

    private void HandleInteractions()
    {
        float interactionDistance = 2f;
        Vector3 moveDirection = GameInput.Instance.GetMovemnetVectorNormalized();

        if (moveDirection != Vector3.zero)
        {
            _lastMoveDirection = moveDirection;
        }
        
        if (Physics.Raycast(transform.position, _lastMoveDirection, out RaycastHit raycastHit, interactionDistance, _countersLayerMask))
        {
            if (raycastHit.transform.TryGetComponent(out BaseCounter baseCounter))
            {
                if (baseCounter != _SelectedCounterInPlayer)
                {
                    SetSelectedCounter(baseCounter);
                }
            }
        }
        else
        {
            _SelectedCounterInPlayer = null;
            SetSelectedCounter(_SelectedCounterInPlayer);
        } 
    }    

    private void SetSelectedCounter ( BaseCounter selectedCounter)
    {
        _SelectedCounterInPlayer = selectedCounter;

        OnSelectedCounterChanged?.Invoke(this, new OnSelectedCounterChangedEventArgs
        {
            _SelectedCounterInEvent = _SelectedCounterInPlayer
        });
    }


    public bool IsWalking()
    {
        if (_playerMovement.GetVelocityMagnitude() <= 1f)
        {
            _isWalking = false;
        }
        else
        {
            _isWalking = true;
        }
        return _isWalking;
    }

    public Transform GetClearCounterTopPoint()
    {
        return _kitchenObjectTopPoint;
    }

    public void SetKitchenObj(KitchenObject kitchenObject)
    {
        _kitchenObject = kitchenObject;
        if (kitchenObject != null)
        {
            OnPickedSomething?.Invoke(this, EventArgs.Empty);
        }
    }
    public KitchenObject GetKitchenObject()
    {
        return _kitchenObject;
    }

    public void ClearKitchenObject()
    {
        _kitchenObject = null;
    }

    public bool HasKitchenObject()
    {
        return _kitchenObject != null;
    }
}
