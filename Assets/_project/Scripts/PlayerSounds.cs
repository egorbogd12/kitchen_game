using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    private float _footStepsTimer;

    [SerializeField,Range(0,1)]private float _footStepsTimerMax = 1f;

    private void Update()
    {
        _footStepsTimer -= Time.deltaTime;
        if (_footStepsTimer < 0)
        {
            _footStepsTimer = _footStepsTimerMax;
            if (Player.Instance.IsWalking())
            {
                float volume = 1f;
                SoundManager.Instance.PlayFootstepsSound(Player.Instance.transform.position, volume);
            }
        }
    }
}
