using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }

    private const string PLAYER_PREFS_SOUND_FX_VOLUME = "SoundEffectsVolume";
    [SerializeField] private AudioClipSO _audioClipSO;
    private float _volume = 1f;
    private void Awake()
    {
        Instance = this;
        _volume = PlayerPrefs.GetFloat(PLAYER_PREFS_SOUND_FX_VOLUME, 1f);
    }

    private void Start()
    {
        DeliveryManager.Instance.OnRecipeSuccess += DeliveryManger_OnRecipeSuccess;
        DeliveryManager.Instance.OnRecipeFailed += DeliveryManager_OnRecipeFailed;
        CuttingCounter.OnAnyCut += CuttingCounter_OnAnyCut;
        Player.Instance.OnPickedSomething += Player_OnPickedSomething;
        BaseCounter.OnAnyObjPlacedHere += BaseCounter_OnAnyObjPlacedHere;
        TrashCounter.OnAnyObjectTrashed += TrashCounter_OnAnyObjectTrashed;
    }

    private void TrashCounter_OnAnyObjectTrashed(object sender, System.EventArgs e)
    {
        TrashCounter trashCounter = sender as TrashCounter;
        PlaySound(_audioClipSO.trash, trashCounter.transform.position);
    }

    private void BaseCounter_OnAnyObjPlacedHere(object sender, System.EventArgs e)
    {
        BaseCounter baseCounter = sender as BaseCounter;
        PlaySound(_audioClipSO.objectDrop, baseCounter.transform.position);
    }

    private void Player_OnPickedSomething(object sender, System.EventArgs e)
    {
        PlaySound(_audioClipSO.objectPickup,    Player.Instance.transform.position );
    }

    private void CuttingCounter_OnAnyCut(object sender, System.EventArgs e)
    {
        CuttingCounter cuttingCounter = sender as CuttingCounter;
        PlaySound( _audioClipSO.chop , cuttingCounter.transform.position);
    }

    private void DeliveryManager_OnRecipeFailed(object sender, System.EventArgs e)
    {
        DeliveryCounter deliveryCounter = DeliveryCounter.Instance;
        PlaySound(_audioClipSO.deliveryFail, deliveryCounter.transform.position);
    }

    private void DeliveryManger_OnRecipeSuccess(object sender, System.EventArgs e)
    {
        DeliveryCounter deliveryCounter = DeliveryCounter.Instance;
        PlaySound(_audioClipSO.deliverySuccess, deliveryCounter.transform.position);
    }



    private void PlaySound(AudioClip[] audioClipArray, Vector3 position, float volume=1f)
    {
        PlaySound(audioClipArray[Random.Range(0, audioClipArray.Length)],  position, volume);

    }
    private void PlaySound(AudioClip audioClip, Vector3 position, float volumeMultiplier = 1f)
    {
        AudioSource.PlayClipAtPoint(audioClip, position, volumeMultiplier * _volume);
    }

    public void PlayFootstepsSound(Vector3 position, float volume)
    {
        PlaySound( _audioClipSO.footstep, position, volume);
    }
    public void PlayCountDownSound()
    {
        PlaySound(_audioClipSO.warning, Vector3.zero);
    }
    public void PlayWarningSound( Vector3 position)
    {
        PlaySound(_audioClipSO.warning, position);
    }

    public void ChangeVolume()
    {
        _volume += 0.1f;
        if (_volume >1f)
        {
            _volume = 0f;
        }
        PlayerPrefs.SetFloat(PLAYER_PREFS_SOUND_FX_VOLUME, _volume);
        PlayerPrefs.Save();
    }

    public float GetVolume()
    {
        return _volume;
    }
}
