using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DeliveryManager : MonoBehaviour
{
   
    public static DeliveryManager Instance { get; private set; }
    public event EventHandler OnRecipeSpawned;
    public event EventHandler OnRecipeCompleted;
    public event EventHandler OnRecipeSuccess;
    public event EventHandler OnRecipeFailed;


    [SerializeField] private RecipeListSO _recipeListSO;
    private List<RecipeSO> _waitingrecipeSOList;
    [SerializeField, Range(3f,10f)] private float _recipeSpawnTimerMax = 4f;
    private int _waitingrecipeSpawnMax = 4;
    private float _recipeSpawnTimer;
    private int _recipeSuccedAmmount = 0;

    private void Awake()
    {
        Instance = this;
        _waitingrecipeSOList = new List<RecipeSO>();
    }

    private void Update()
    {
        _recipeSpawnTimer -= Time.deltaTime;
        if (_recipeSpawnTimer <= 0)
        {
            _recipeSpawnTimer = _recipeSpawnTimerMax;
            if ( GameManager.Instance.IsGamePlaying() && _waitingrecipeSOList.Count < _waitingrecipeSpawnMax)
            {
                RecipeSO waitnigRecipeSO = _recipeListSO.RecipeSOList[UnityEngine.Random.Range(0, _recipeListSO.RecipeSOList.Count)];
                _waitingrecipeSOList.Add(waitnigRecipeSO);
                OnRecipeSpawned?.Invoke(this, EventArgs.Empty);
                
            }
        }
    }

    public void DeliverRecipe(PlateKitchenObject plateKitchenObject)
    {
        for (int i = 0; i < _waitingrecipeSOList.Count; i++)
        {
            RecipeSO waitingRecipeSO = _waitingrecipeSOList[i];
            if (waitingRecipeSO.KitchenObjSOList.Count == plateKitchenObject.GetKitchenObjSOList().Count) // �������� �� ���������� ���-�� ����������� �� �������
            {
                bool plateContentsMatchesRecipe = true;
                foreach (KitchenObjSO recipeKitchenObjectSO in waitingRecipeSO.KitchenObjSOList) // ��� ���������� � �������
                {
                    bool ingridientFound = false;
                    foreach (KitchenObjSO kitchenObjSO in plateKitchenObject.GetKitchenObjSOList()) // ��� ���������� �� �������
                    {
                        if (recipeKitchenObjectSO == kitchenObjSO)
                        {
                            ingridientFound = true;
                            break;
                        }
                    }
                    if (!ingridientFound) // �� ������ �� �������
                    {
                        plateContentsMatchesRecipe = false;
                    }
                }
                if (plateContentsMatchesRecipe) // ���������� ������ ������
                {
                    _recipeSuccedAmmount++;
                    _waitingrecipeSOList.RemoveAt(i);
                    OnRecipeCompleted?.Invoke(this, EventArgs.Empty);
                    OnRecipeSuccess?.Invoke(this, EventArgs.Empty);
                    return;
                }
            }
        }
        OnRecipeFailed?.Invoke(this, EventArgs.Empty);
    }

    public List <RecipeSO> GetRecipeSOList()
    {
        return _waitingrecipeSOList;
    }

    public int GetSuccessRecipesAmmount()
    {
        return _recipeSuccedAmmount;
    }
}
