using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenObject : MonoBehaviour
{
    [SerializeField] private KitchenObjSO _kitchenObj;

    private IKitchenObjectParent _kitchenObjectParent;

    public KitchenObjSO GetKitchenObjSO()
    { return _kitchenObj; }

    public void SetKitchenObjectParent( IKitchenObjectParent kitchenObjectParent)
    {
        if (_kitchenObjectParent != null) // ������� ������ �����(clearCounter)
        {
            _kitchenObjectParent.ClearKitchenObject();
        }
        _kitchenObjectParent = kitchenObjectParent; // ������������� ����� �����(clearCounter) ��� �������

        if (kitchenObjectParent.HasKitchenObject()) // �������� ����� �� ����� �� ���� ������
        {
            Debug.LogError("est kitchen obj");
        }

        kitchenObjectParent.SetKitchenObj(this); 

        transform.parent = kitchenObjectParent.GetClearCounterTopPoint(); //
        transform.localPosition = Vector3.zero;
    }    

    public IKitchenObjectParent GetKitchenObjectParent()
    {
        return _kitchenObjectParent;
    }
    
    public void DestroySelf()
    {
        _kitchenObjectParent.ClearKitchenObject();
        Destroy(gameObject);
    }

    public bool TryGetPlate(out PlateKitchenObject plateKitchenObject)  // ��������. ����� �������� �������� ��� ��� "PlateKitchenObject ",����� �������� �� ���
    {
        if (this is PlateKitchenObject) // �������� �� ���
        {
            plateKitchenObject = this as PlateKitchenObject; // �������� �������
            return true;
        }
        else // �������� �� ��������
        {
            plateKitchenObject = null; // ����������� �������� �������� �������� ���� ���������� � ���� ��
            return false;
        }
    }


    public static KitchenObject SpawnKitchenObject(KitchenObjSO kitchenObjSO, IKitchenObjectParent kitchenObjectParent)
    {
        Transform kitchenObjTransform = Instantiate(kitchenObjSO.prefab);

        KitchenObject kitchenObj = kitchenObjTransform.GetComponent<KitchenObject>();
        kitchenObj.SetKitchenObjectParent(kitchenObjectParent); // ������� � ������ ������� � �������� �������� �������
        return kitchenObj;
    }
}
