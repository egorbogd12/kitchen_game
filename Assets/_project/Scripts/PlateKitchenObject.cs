using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlateKitchenObject : KitchenObject
{
    [SerializeField] private List<KitchenObjSO> _validKitchenObjectSOList;

    private List<KitchenObjSO> _kitchenObjSOList;


    public event EventHandler <OnIngridentAddedEventArgs> OnIngridentAdded;
     public class OnIngridentAddedEventArgs : EventArgs
    {
        public KitchenObjSO kitchenObjSOEvent;
    }

    private void Awake()
    {
        _kitchenObjSOList = new List<KitchenObjSO>();
    }

    public bool TryAddIngridient (KitchenObjSO kitchenObjSO)
    {
        if (!_validKitchenObjectSOList.Contains(kitchenObjSO)) // �������� ������ �� �������� �� �������.
        {
            return false;
        }
        else
        {
            if (_kitchenObjSOList.Contains(kitchenObjSO)) // �������� �� ��������
            {
                return false;
            }
            else
            {
                _kitchenObjSOList.Add(kitchenObjSO);
                OnIngridentAdded?.Invoke(this, new OnIngridentAddedEventArgs { kitchenObjSOEvent = kitchenObjSO });
                return true;
            }
        }
    }

    public List<KitchenObjSO> GetKitchenObjSOList()
    {
        return _kitchenObjSOList;
    }
    
}
