using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKitchenObjectParent 
{
    public Transform GetClearCounterTopPoint();


    public void SetKitchenObj(KitchenObject kitchenObject);

    public KitchenObject GetKitchenObject();
    public void ClearKitchenObject();

    public bool HasKitchenObject();
}
