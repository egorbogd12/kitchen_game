using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 5f;
    [SerializeField] private float _rotationSpeed = 5f;

    private Rigidbody _rb;
    private Vector3 _movement;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        _movement = GameInput.Instance.GetMovemnetVectorNormalized();
        _rb.AddForce(_movement * _speed);

        if (_movement != Vector3.zero)
        {
            Quaternion moveDir = Quaternion.Lerp(_rb.rotation, Quaternion.LookRotation(_movement), _rotationSpeed * Time.deltaTime);
            _rb.MoveRotation(moveDir);
        }
    }

    public float GetVelocityMagnitude()
    {
        return _rb.velocity.magnitude;
    }
}
