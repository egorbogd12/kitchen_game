using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class FiringRecipeSO : ScriptableObject
{
    public KitchenObjSO _objInput;
    public KitchenObjSO _objOutput;
    public float FiringProgressMax;

}
