using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class BurningRecipeSO : ScriptableObject
{
    public KitchenObjSO _objInput;
    public KitchenObjSO _objOutput;
    public float BurningProgressMax;

}
