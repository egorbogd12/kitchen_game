using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class CuttingRecipeSO : ScriptableObject
{
    public KitchenObjSO _objInput;
    public KitchenObjSO _objOutput;
    public int cuttingProgressMax;

}
