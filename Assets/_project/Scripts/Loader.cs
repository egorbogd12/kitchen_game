using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Loader 
{
    private static Scene _targetScene;

    public enum Scene
    {
        MainMenuScene,
        LoadingScene,
        MainScene,
    }
    public static void Load(Scene sceneName)
    {
        Loader._targetScene = sceneName;
        SceneManager.LoadScene(Scene.LoadingScene.ToString());
        
    }

    public static void LoaderCallback()
    {
        SceneManager.LoadScene(_targetScene.ToString());
    }
}
